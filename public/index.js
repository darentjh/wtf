(function() {

    var MySiteApp = angular.module("MySiteApp", ["http-auth-interceptor", "ui.router", "MyControllers"]);

    var MySiteConfig = function($httpProvider, $stateProvider, $urlRouterProvider) {
        $httpProvider.defaults.withCredentials = true;
        $stateProvider
            .state("main", {
                url: "/main",
                templateUrl: "/state/main.html"
            })
            
            .state("login", {
                url: "/login",
                templateUrl: "/state/login.html",
                controller: "LoginCtrl as loginCtrl"
                // controllerAs: "loginCtrl"
            })
            
            .state("protected_checkout", {
                url: "/protected/checkout",
                templateUrl: "/protected/state/checkout.html",
                controller: "LogOutCtrl as logoutCtrl"
            })
            
            .state("shop", {
                url: "/shop",
                templateUrl: "state/shop.html",
                controller: "ListingsCtrl as listingsCtrl"
            })
            
            .state("about", {
                url: "/about",
                templateUrl: "state/about.html"
            })
            
            .state("zine", {
                url: "/zine",
                templateUrl: "state/zine.html"
            })
            
            .state("stories", {
                url: "/stories",
                templateUrl: "state/stories.html"
            })
            
            .state("register", {
                url: "/register",
                templateUrl: "state/register.html",
                controller: "RegisterCtrl as registerCtrl"
            })

            .state("registered", {
                url: "/registered",
                templateUrl: "state/registered.html"
            })
            
            .state("details", {
                url:"/product/:product_name",
                templateUrl: "state/details.html",
                controller: "DetailsCtrl as detailsCtrl"
            })
            
            .state("contact", {
                url:"/contact",
                templateUrl: "state/contactus.html"
            })
        
            .state("addedtocart", {
                url:"/addedtocart",
                templateUrl: "protected/state/addedtocart.html"
            })

            .state("cart", {
                url:"/cart",
                templateUrl: "protected/state/cart.html",
                controller:  "CartCtrl as cartCtrl"
            })

            .state("authoring", {
                url: "/authoring",
                templateUrl: "/state/authoring.html",
                controller:  "AuthoringCtrl as authoringCtrl"
            })

            .state("posts", {
                url: "/posts",
                templateUrl: "/state/posts.html",
                controller:  "PostsCtrl as postsCtrl"

            });

        $urlRouterProvider.otherwise("/main");
    };


    MySiteApp.config(["$httpProvider", "$stateProvider", "$urlRouterProvider", MySiteConfig]);
})();