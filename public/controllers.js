/**
 * Created by Daren on 19/5/16.
 */
(function() {

    var MyControllers = angular.module("MyControllers", ["ui.router"]);

    var ListingsCtrl = function ($http, $state) {

        var vm = this;
        vm.cart = [];

        vm.gotoDetails = function (pname) {
            $state.go("details",
                {product_name: pname}
            )
        };

        $http.get("/shop")
            .then(function (result) {
                vm.shop = result.data;
                console.log(result);

            }).catch(function (err) {
            console.error(">>error: %s", err);
        });
    };

    var DetailsCtrl = function($http, $state, $stateParams) {
        var vm = this;
        vm.product = {};
        console.log($stateParams.product_name);

        $http.get("/shop/" + $stateParams.product_name)
            .then(function(result) {

                vm.product = result.data
            }).catch(function(err) {
            console.error(">> error: $s", err);
        });
    };
    
    var LoginCtrl = function($http, $httpParamSerializerJQLike, authService) {
        var vm = this;
        vm.email = "";
        vm.password = "";
        vm.login = function() {
            $http({
                url: "/authenticate",
                method: "POST",
                data: $httpParamSerializerJQLike({
                    email: vm.email,
                    password: vm.password
                }),
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(function() {
                authService.loginConfirmed(vm.email);
            });
        }
    };

    var LogOutCtrl = function($http, $templateCache, $state, authService) {
        var vm = this;
        vm.logout = function () {
            $http.get("/logout")
                .then(function() {
                    //ui.router caches views in $templateCache. Need to remove the cache page
                    //when we logout otherwise the cache page will be redisplayed
                    $templateCache.remove("/protected/state/checkout.html");
                    $state.go("main");
                });
        }
    };

    var ViewsLoginCtrl = function($scope, $state, authService) {

        var vm = this;

        vm.status = {
            message: ""
        };

        //401
        $scope.$on("event:auth-loginRequired", function() {
            vm.status.message = "Please login";
            $state.go("login");
        });

        //200
        $scope.$on("event:auth-loginConfirmed", function(_, name) {
            vm.status.message = "Hello " + name;
            $state.go("protected_checkout");
        });

        //403
        $scope.$on("event:auth-forbidden", function() {
            vm.status.message = "Please username/password. Please try again";
            authService.loginConfirmed();
        });

        $scope.$on("event:auth-loginCancelled", function() {

        });

    };
    
    var RegisterCtrl = function($http, $state) {
        
        var vm = this;

        vm.register = function() {
            var data = {};
            
            $http({
                method: 'POST',
                url: '/postregisterform',
                data: {
                    email: vm.email,
                    password: vm.password,
                    cfmpassword: vm.cfmpassword,
                    firstname: vm.firstname,
                    lastname: vm.lastname
                }
            })
                
            .success(function() {
                console.log('success data sent: ' + data);
                $state.go('registered');
            });
        };
    };

    var AuthoringCtrl = function($http, $state) {

        var vm = this;

        vm.authoringPost = function() {
            var data = {};

            $http({
                method: 'POST',
                url: '/createpost',
                data: {
                    createPostTitle: vm.createPostTitle,
                    createPostAuthor: vm.createPostAuthor,
                    createPostPhotog: vm.createPostPhotog,
                    createPostDate: vm.createPostDate,
                    createPostSummary: vm.createPostSummary,
                    createPostContent: vm.createPostContent
                }
            })
                .success(function() {
                    console.log('success data sent: ' + data);
                    $state.go('posts');
                });
        };
    };

    var PostsCtrl = function ($http, $state) {

        var vm = this;

        $http.get("/posts")
            .then(function (result) {
                vm.posts = result.data;
                console.log(result);

            }).catch(function (err) {
            console.error(">>error: %s", err);
        });
    };
    
    MyControllers.controller("ListingsCtrl", ["$http", "$state", ListingsCtrl]);
    MyControllers.controller("DetailsCtrl", ["$http", "$state", "$stateParams", DetailsCtrl]);
    MyControllers.controller("LoginCtrl", ["$http", "$httpParamSerializerJQLike", "authService", LoginCtrl]);
    MyControllers.controller("LogOutCtrl", ["$http", "$templateCache", "$state", "authService", LogOutCtrl]);
    MyControllers.controller("ViewsLoginCtrl", ["$scope", "$state", "authService", ViewsLoginCtrl]);
    MyControllers.controller("RegisterCtrl", ["$http", "$state", RegisterCtrl]);
    MyControllers.controller("AuthoringCtrl", ["$http", "$state", AuthoringCtrl]);
    MyControllers.controller("PostsCtrl", ["$http", "$state", PostsCtrl]);

})();

