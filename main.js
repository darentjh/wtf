/**
 * Created by Daren on 15/5/16.
 */
var express = require("express");
var app = express();

var morgan = require('morgan');
app.use(morgan('combined'));

var config = require("./modules/dbconfig");
var mysql = require("mysql");
var connPool = mysql.createPool(config);

var error = function(code, req, res) {
    res.status(code);
    res.type("text/plain");
    res.end();
};

var api_key = "key-2675229d8325be00bd3f86511233bde9";
var domain = 'mailgun.acuriouscuration.com';
var Mailgun = require('mailgun-js');

var session = require("express-session");

var bodyParser = require("body-parser");

var bcrypt = require('bcrypt-nodejs');
var ensure = require("connect-ensure-login");
var passport = require("passport");
var PassportLocal = require("passport-local");
var local = new PassportLocal(
    { usernameField: "email", 
      passwordField: "password", 
      passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) {
        connPool.getConnection(function (err, conn) {
            conn.query("SELECT * FROM register WHERE email = ?", [email], function (err, rows) {
                if (err)
                    return done(err);
                if (!rows.length) {
                    return done(null, false);
                }
                return done(null, rows[0]);
            });
        });
    });


passport.use(local);
passport.serializeUser(function(user, done) {
    done(null, user);
});
passport.deserializeUser(function(id, done) {
    done(null, {email: id});
});


app.use(session({
    secret: "hello",
    resave: false,
    saveUninitialized: false
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.use(passport.initialize());
app.use(passport.session());


app.use("/protected/*", ensure.ensureLoggedIn("/status/401"));


app.post("/authenticate", passport.authenticate("local", {
    successRedirect: "/status/200",
    failureReturnToOrRedirect: "/status/403"
}));


app.get("/status/:code", function(req, res) {
    res.sendStatus(parseInt(req.params.code));
});


app.get("/logout", function(req, res) {
    req.logout();
    req.session.destroy();
    res.sendStatus(200);
});


app.get("/shop", function(req, res){
    connPool.getConnection(function(err, conn) {
        //If there are error, report it
        if (err) {
            error(400, err, res);
            return;
        }
        //Perform the query
        try {
            conn.query("select * from vendorUpload", 
                function (err, rows) {
                    if (err) {
                        error(400, err, res);
                        return;
                    }
                    //Render the template with rows as the context
                    console.info(">> request type: %s", req.headers["Content-Type"]);
                    res.format({
                        "text/html": function() {
                            res.render("shop", { vendorUpload: rows });
                        },
                        "application/json": function() {
                            res.json(rows);
                        }
                    })

            });
        } catch (ex) {
            error(400, ex, res);
        } finally {
            conn.release();
        }
    });
});


app.get("/shop/:product_name", function(req, res) {
    connPool.getConnection(function(err, conn) {
        try {
            conn.query("SELECT * from vendorUpload where product_name = ?", [req.params.product_name],
                function(err, rows) {
                    if (err) {
                        console.error("Query has error: %s", err);
                        error(400, err, res);
                        return;
                    }
    
                    if(rows.length)
                        res.json(rows[0]);
                    else {
                        console.error("Query has error: %s", err);
                        res.status(400).end(JSON.stringify(err));
                        // res.render("/error");
                    }
                });
        } catch(ex) {}
        finally {
            conn.release();
        }
    });
});


app.post('/postregisterform', function(req,res) {
    var mailgun = new Mailgun({apiKey: api_key, domain: domain});
    
    var registerSuccess = {
        from: 'A Curious Curation <daren@acuriouscuration.com>',
        to: 'daren@acuriouscuration.com',
        subject: 'Welcome to A Curious Curation',
        text: 'You have been successfully registered!'
    };

    mailgun.messages().send(registerSuccess, function (error, body) {
        console.log(body);
    });

    console.log(req.body);
    connPool.getConnection(function (err, conn) {
        //If there are error, report it
        if (err) {
            console.error("get connection error: %s", err);
            error(400, err, res);
            return;
        }
        // try {
            var paramInsert = "INSERT INTO register (email, password, cfmpassword, firstname, lastname) VALUES (?,?,?,?,?)";
            conn.query(paramInsert,
                [req.body.email,req.body.password,req.body.cfmpassword,req.body.firstname,req.body.lastname],
                function (err, rows) {
                    if (err) {
                        console.error("post has error: %s", err);
                        error(400, err, res);
                        return;
                    }
                    //Render the template with rows as the context
                    res.sendStatus(200);
                    // res.end('sent');
                    console.log('sent');
                    conn.release();
                });
    });
});

app.post('/createpost',
    function (req, res) {
        console.info(">>Post Request Sent");
        connPool.getConnection(function (err, conn) {
            //If there are error, report it
            if (err) {
                console.error("get connection error: %s", err);
                error(400, err, res);
                return;
            }
            // try {
                var paramInsert = "INSERT INTO postsform values (?,?,?,?,?,?)";
                conn.query(paramInsert,
                    [req.body.createPostTitle, req.body.createPostAuthor, req.body.createPostPhotog, req.body.createPostDate, req.body.createPostSummary, req.body.createPostContent],
                    function (err, rows) {
                        if (err) {
                            console.error("post has error: %s", err);
                            error(400, err, res);
                            return;
                        }
                        //Render the template with rows as the context
                        res.sendStatus(200);
                    });
                conn.release();
        });
    });

app.get("/posts", function(req, res){
    connPool.getConnection(function(err, conn) {
        //If there are error, report it
        if (err) {
            error(400, err, res);
            return;
        }
        //Perform the query
        try {
            conn.query("SELECT * FROM postsform",
                function (err, rows) {
                    if (err) {
                        error(400, err, res);
                        return;
                    }
                    //Render the template with rows as the context
                    console.info(">> request type: %s", req.headers["Content-Type"]);
                    res.format({
                        "text/html": function() {
                            res.render("posts", { postsform: rows });
                        },
                        "application/json": function() {
                            res.json(rows);
                            console.log(rows);
                        }
                    })
                });
        } catch (ex) {
            error(400, ex, res);
        } finally {
            //IMPORTANT! Release the connection
            conn.release();
        }
    });
});

app.use(express.static(__dirname + "/public"));
app.use("/assets", express.static(__dirname + "/assets"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));


//Handle file not found - 400
app.use(function(req, res, next) {
    res.status(404);
    res.type("text/plain");
    res.send("File not found: " + req.originalUrl);
});


//Error - 500
app.use(function(err, req, res, next) {
    console.error("Application error: %s", err);
});


app.set("port", process.env.APP_PORT || 3000);
app.listen(app.get("port"), function () {
    console.info("WTF App is listening on Port %s", app.get("port"));
});